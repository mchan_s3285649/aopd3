package agents;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.NavigableSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import au.rmit.ract.planning.pathplanning.entity.ComputedPlan;
import au.rmit.ract.planning.pathplanning.entity.State;
// import au.rmit.ract.planning.pathplanning.entity.Plan;
import au.rmit.ract.planning.pathplanning.entity.SearchNode;
import pplanning.interfaces.PlanningAgent;
import pplanning.simviewer.model.GridCell;
import pplanning.simviewer.model.GridCoord;
import pplanning.simviewer.model.GridDomain;

// (2010) RICHTER - The Joy of Forgetting: Faster Anytime Search via Restarting

interface Heuristic
{
	float h(final State start, final State goal);
}

public class MyCoolAgent implements PlanningAgent
{
	protected ComputedPlan path = null;
    protected int	stepNo;
	
	// It's quicker to create the objects once only.
	/* If h() is cheap, then seen can probably be a set of States.
	 * If h() is NOT cheap, then it needs to be a set of SearchNodes with h() stored.
	*/
	private final NavigableSet<State>	open = new TreeSet<State>(new LowestCost());
	private final Map<State, SearchNode> node = new HashMap<State, SearchNode>();
	private final Set<State> seen = new HashSet<State>();
	private final Set<State> closed = new HashSet<State>();
	
	// For debugging.
	private Set<State> expanded = null;
	private Set<State> unexpanded = null;
    // WEIGHT_I and PHI value taken from article, roughly.
    private final float WEIGHT_I = 50, PHI = 2.0f / 3.0f;
    
    private final int PATH_COST_INDEX = 0; // For node cost constructor
	
    GridDomain MAP = null;
    GridCell START = null, GOAL = null;
    
    private float weight;
    Heuristic heuristic;
    
	// final double octile_magic = Math.sqrt(2) - 1;
	final double octile_magic = 0.1;
	double prevTimeLeft = 0;
	float minCost;

    
	// You need to re-implement this method with your own strategy
	@Override
	public GridCell getNextMove(GridDomain map, GridCell sState,
			GridCell gState, int stepLeft, long stepTime, long timeLeft)
	{
		boolean replan = false;
		if (GOAL != gState) // New goal, everything resets.
		{
			System.out.println("*** REPLAN: Goal changed location.");
			replan = true;
			seen.clear();
		}
		else
			if(map.getChangedEdges().size() > 0)
			{
				System.out.println("*** REPLAN: Map changed.");
				replan = true;
			}
			else
				if(timeLeft > prevTimeLeft)
				{
					System.out.println("*** REPLAN: timeLeft magically increased!");
					replan = true;
				}
		
		if(replan)
		{
			if(GOAL == null)
			{
				final int children = map.getSuccessors(gState).size();
				minCost = map.getMinCost();
				
				System.out.println("Children: " + children + ", minCost: " + minCost + ", octile magic: " + octile_magic);

				switch (children)
				{
					case 4:
						heuristic = new Octile();
						break;
						
					case 8:
						heuristic = new Octile();
						break;
						
					default:
						throw new RuntimeException("" + children + " children: WTF?");
				}
			}
			
			System.out.println("START: " + sState + ", GOAL: " + gState + ". TIMELEFT: " + timeLeft + " ms");
			
			// Using Java 1.7, there is now a measurable amount of time required to break out of the loop and 
			// process the path to be returned.
			final long plan_time = timeLeft - 50;
			MAP = map;
			START = sState;
			GOAL = gState;
			weight = WEIGHT_I;
			ComputedPlan tmp = plan(map, sState, gState, plan_time);
			if(tmp != null)
			{
				path = tmp;
				stepNo = 0;
			}
		}
		
		prevTimeLeft = timeLeft;
		
		return path != null ? (GridCell) path.getStep(++stepNo) : null;
	}


	private ComputedPlan plan(final GridDomain map, final GridCell sState, final GridCell gState, long plan_time)
	{
        long startTime = System.nanoTime();
		ComputedPlan new_path = null;

		float bound = Float.MAX_VALUE;
		boolean failed = false;
		boolean interrupted = false;

		while (!interrupted && !failed)
		{
			SearchNode goal = null;
			
			closed.clear();
			open.clear();
			final SearchNode START = new SearchNode(sState);
			START.setParent(null);
			START.set(PATH_COST_INDEX, 0);
			node.put(sState, START);
			open.add(sState);
			
			System.out.println("SEARCHING (weight: " + weight + ")...");

			while (!interrupted && !open.isEmpty() && goal == null)
			{
				final State s = open.pollFirst();
				final SearchNode parent = node.get(s);
				final ArrayList<State> successors = map.getSuccessors(s);
				closed.add(s);

				for (final State sPrime : successors)
				{
					final float cur_g = g(parent) + transition_cost(s, MAP, sPrime);

					/*
					final Float F = f(sPrimeNode, map, sState, gState);
					if (F >= bound)
					{
						if (!F.isInfinite())
							System.out.println("CONTINUE(" + bound + "): " + sPrimeNode);
						continue;
					}
					*/
					
					
					if (cur_g >= bound)
						continue;
					
					final SearchNode child = node.get(sPrime);
					final Float old_g = child != null ? child.get(PATH_COST_INDEX) : null;
					final boolean in_seen = seen.contains(sPrime);
					SearchNode sPrimeNode;
					
					if (in_seen)
					{
						sPrimeNode = node.get(sPrime); 

						if (cur_g < old_g)
						{
							// Update.
							sPrimeNode.set(PATH_COST_INDEX, cur_g);
							sPrimeNode.setParent(parent);
						}
						else
						{
							// What we have seen before is no worse: do nothing.
						}
						
						seen.remove(sPrime);
						open.add(sPrime);
					}
					else
					{
						// We can cheat and use old_g, because if it is NOT in seen OR node,
						// then it is not in anything.
						// But if it is in node, then it is in Open or Closed.

						if (old_g == null)
						{
							sPrimeNode = GenerateNode(sPrime, s, cur_g);
							node.put(sPrime, sPrimeNode); // Add.
							open.add(sPrime);
						}
						else
						{
							sPrimeNode = node.get(sPrime); 
							
							if (cur_g < old_g)
							{
								// Update.
								sPrimeNode.set(PATH_COST_INDEX, cur_g);
								sPrimeNode.setParent(parent);

								// Open is likely to be much smaller than Closed as the weight nears 1, 
								// which even for a HashMap can make a performance difference.
								if (!open.contains(sPrime))
								{
									closed.remove(sPrime);
									open.add(sPrime);
								}
							}
							else
							{
								// If cur_g is no better, do nothing.
							}
						}
					}
															
					if (sPrime == gState)
					{
						goal = sPrimeNode;
						break; // This only breaks out of the for loop of successors of s.
					}

					final long now = System.nanoTime();
					if ((now - startTime) / 1000000 >= plan_time)
					{
						System.out.println("Interrupted after " + (now - startTime) / 1000000 + " ms");
						interrupted = true;
						break;
					}

				}
			}
			
			if (goal != null)
			{
				System.out.println("GOAL (" + (System.nanoTime() - startTime) / 1000000 + " ms): " + goal + "(" + open.size() + ", " + closed.size() + ")");
				bound = goal.get(PATH_COST_INDEX);
				// System.out.println("NEW BOUND: " + bound);
				new_path = new ComputedPlan();
				while (goal.getParent() != null)
				{
					new_path.prependStep(goal.getNode());
					goal = goal.getParent();
				}

				new_path.prependStep(sState);
				
				weight = Math.max(1.0f, weight * PHI);
				seen.addAll(closed);
				seen.addAll(open);
				expanded = new HashSet<State>(closed);
				unexpanded = new HashSet<State>(open);
			}
			else
			{
				if (!interrupted)
				{
					failed = true;
					System.out.print("FAILED (" + open.size() + ", " + closed.size() + ").  :(  ");
					if (open.isEmpty())
						System.out.println("Open is empty.");
					else
						System.out.println("But why??");
				}
			}
			
		}
		
		return new_path;
	}

	
	private SearchNode GenerateNode(final State sPrime, final State s, final float path_cost)
	{
		final SearchNode child = new SearchNode(sPrime), parent = node.get(s);
		child.set(PATH_COST_INDEX, path_cost);
		child.setParent(parent);
		// sPrimeNode.set(FPRIME_INDEX, fPrime(sPrimeNode, MAP, GOAL, weight));
		return child;
	}
		
		
	// Return the cost to transition from s => sPrime.
	private float transition_cost(final State s, final GridDomain map, final State sPrime)
	{
		return map.cost(s, sPrime);
	}

	// Calculate g() + h()
	float f(final SearchNode sPrimeNode, final GridDomain map, GridCell sState, GridCell gState)
	{
		return g(sPrimeNode) + heuristic.h(sPrimeNode.getNode(), gState);
	}
	

	float fPrime(final SearchNode sPrimeNode, final GridDomain map, GridCell gState, float w)
	{
		return g(sPrimeNode) + w * heuristic.h(sPrimeNode.getNode(), gState);
	}
	

	// Return the path cost for SearchNode s.
	float g(final SearchNode s)
	{
		return s.get(PATH_COST_INDEX);
	}

	
	@SuppressWarnings("unused")
	private class Default implements Heuristic
	{
		public float h(State start, State goal)
		{
			final float result = MAP.hCost(start, goal);
			return result;
		}
	}
	
	// octile heuristic is max(dx, dy) + (sqrt(2)-1) * min(dx, dy)
	@SuppressWarnings("unused")
	private class Octile implements Heuristic
	{
		public float h(final State start, final State goal)
		{
			final GridCoord A = ((GridCell)start).getCoord();
			final GridCoord B = ((GridCell)goal).getCoord();
			final int dx = Math.abs(B.getX() - A.getX()), dy = Math.abs(B.getY() - A.getY());
			final float result = (float) (Math.max(dx, dy) + octile_magic * Math.min(dx, dy));
			// final float result = (float)Math.max(dx, dy);
			return minCost * result;
		}
	}

	
	private class Manhattan implements Heuristic
	{
		public float h(final State start, final State goal)
		{
			final GridCoord A = ((GridCell)start).getCoord();
			final GridCoord B = ((GridCell)goal).getCoord();
			final int dx = Math.abs(B.getX() - A.getX()), dy = Math.abs(B.getY() - A.getY());
			final float result = dx + dy; 
			return minCost * result;
		}
	}

	
	// Why is this so much slower than the one in Apparate???
	@SuppressWarnings("unused")
	private class Euclidean implements Heuristic
	{
		@Override
		public float h(State start, State goal)
		{
			final GridCoord A = ((GridCell)start).getCoord();
			final GridCoord B = ((GridCell)goal).getCoord();
			final float result = (float) Math.sqrt(Math.pow(B.getY() - B.getX(), 2) + Math.pow(A.getY() - A.getX(), 2));
			return minCost * result;
		}	
	}
	
	
	// Comparator for open list.
	private class LowestCost implements Comparator<State>
	{
		@Override
		public int compare(final State A, final State B)
		{
			final float LEFT = fPrime(node.get(A), MAP, GOAL, weight), 
						RIGHT = fPrime(node.get(B), MAP, GOAL, weight);
			return LEFT == RIGHT ? tiebreakonh(A, B) : (LEFT < RIGHT ? -1 : 1);
		}
	}

	
	int tiebreakonh(final State A, final State B)
	{
		final float Ah = heuristic.h(A, GOAL);
		final float Bh = heuristic.h(B, GOAL);
		
		return Ah == Bh ? 0 : (Ah < Bh ? -1 : 1);
	}


	// You can if you want implement the methods below

	// Do we want to show extra info? (e.g., close and open nodes, current path)
	@Override
	public Boolean showInfo()
	{
		return true;
	}

	@Override
	public ArrayList<GridCell> expandedNodes()
	{
		final ArrayList<GridCell> result = new ArrayList<GridCell>();
		if (expanded != null)
			for (final State S : expanded)
				result.add((GridCell)S);		
		return result;
	}

	@Override
	public ArrayList<GridCell> unexpandedNodes()
	{
		final ArrayList<GridCell> result = new ArrayList<GridCell>();
		if (expanded != null)
			for (final State S : unexpanded)
				result.add((GridCell)S);		
		return result;
	}

	@Override
	public ComputedPlan getPath()
	{
		return path;
	}

}
